import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {StoreModel} from '../models/store.model';
import {MainService} from './main.service';

@Injectable({
  providedIn: 'root'
})
export class StoresService extends MainService<StoreModel> {

  constructor(protected http: HttpClient) {
    super('stores', ['currency']);
  }
}
