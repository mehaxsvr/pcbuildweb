import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CaptionModel} from '../../../models/caption.model';
import {TypeModel} from '../../../models/type.model';
import {FieldModel} from '../../../models/field.model';
import {CaptionsService} from '../../../services/captions.service';

@Component({
  selector: 'app-captions',
  templateUrl: './captions.component.html',
  styleUrls: ['./captions.component.scss']
})
export class CaptionsComponent implements OnInit {
  @Input() models: CaptionModel[];
  @Input() types: TypeModel[];
  @Input() fields: FieldModel[];
  editableModel: CaptionModel = null;
  selectedParentId = 0;
  selectedTypeId = 0;
  selectedFieldId = 0;

  @Output() OnChange: EventEmitter<boolean> = new EventEmitter();

  constructor(private captionsService: CaptionsService) { }

  ngOnInit() {
  }

  public setEditable(model?: CaptionModel): void {
    this.editableModel = {
      id: model ? model.id : null,
      is_main: model ? model.is_main : false,
      parent_id: model ? model.parent_id : this.selectedParentId,
      type_id: model ? model.type_id : this.selectedTypeId,
      field_id: model ? model.field_id : this.selectedFieldId,
      value: model ? model.value : ''
    };
  }

  public isHidden(model: CaptionModel): boolean {
    // tslint:disable:triple-equals
    return !(
        (this.selectedFieldId == 0 || this.selectedFieldId == model.field_id) &&
        (this.selectedParentId == 0 || this.selectedParentId == model.parent_id) &&
        (this.selectedTypeId == 0 || this.selectedTypeId == model.type_id)
    );
  }

  public reset(): void {
    this.editableModel = null;
  }

  public save(): void {
    if (this.editableModel.value) {
      if (this.editableModel.id) {
        this.captionsService.update(this.editableModel.id, this.editableModel).subscribe(() => {
          this.OnChange.emit(true);
        });
      } else {
        this.captionsService.create(this.editableModel).subscribe(() => {
          this.OnChange.emit(true);
        });
      }
    }
  }

  public delete(model: CaptionModel): void {
    this.captionsService.delete(model.id).subscribe(() => {
      this.OnChange.emit(true);
    });
  }

  public formKeyPress(e): void {
    switch (e.keyCode) {
      case 13: // Enter
        this.save();
        break;

      case 27: // ESC
        this.reset();
        break;
    }
  }
}
