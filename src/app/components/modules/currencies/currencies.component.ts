import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CurrencyModel} from '../../../models/currency.model';
import {CurrenciesService} from '../../../services/currencies.service';

@Component({
  selector: 'app-currencies',
  templateUrl: './currencies.component.html',
  styleUrls: ['./currencies.component.scss']
})
export class CurrenciesComponent implements OnInit {
  @Input() models: CurrencyModel[];
  editableModel: CurrencyModel = null;

  @Output() OnChange: EventEmitter<boolean> = new EventEmitter();

  constructor(private currenciesService: CurrenciesService) { }

  ngOnInit() {
  }

  public setEditable(model?: CurrencyModel): void {
    this.editableModel = {
      id: model ? model.id : null,
      name: model ? model.name : '',
      short: model ? model.short : '',
      to_eur: model ? model.to_eur : 1,
    };
  }

  public reset(): void {
    this.editableModel = null;
  }

  public save(): void {
    if (this.editableModel.name) {
      if (this.editableModel.id) {
        this.currenciesService.update(this.editableModel.id, this.editableModel).subscribe(() => {
          this.OnChange.emit(true);
        });
      } else {
        this.currenciesService.create(this.editableModel).subscribe(() => {
          this.OnChange.emit(true);
        });
      }
    }
  }

  public formKeyPress(e): void {
    switch (e.keyCode) {
      case 13: // Enter
        this.save();
        break;

      case 27: // ESC
        this.reset();
        break;
    }
  }
}
