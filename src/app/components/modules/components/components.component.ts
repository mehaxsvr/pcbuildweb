import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ComponentModel} from '../../../models/component.model';
import {TypeModel} from '../../../models/type.model';
import {ComponentsService} from '../../../services/components.service';

@Component({
  selector: 'app-components',
  templateUrl: './components.component.html',
  styleUrls: ['./components.component.scss']
})
export class ComponentsComponent implements OnInit {
  @Input() models: ComponentModel[];
  @Input() types: TypeModel[];
  editableModel: ComponentModel = null;

  @Output() OnChange: EventEmitter<boolean> = new EventEmitter();

  constructor(private componentsService: ComponentsService) { }

  ngOnInit() {
  }

  public setEditable(model?: ComponentModel): void {
    this.editableModel = {
      id: model ? model.id : null,
      type_id: model ? model.type_id : 1,
      name: model ? model.name : '',
    };
  }

  public reset(): void {
    this.editableModel = null;
  }

  public save(): void {
    if (this.editableModel.name) {
      if (this.editableModel.id) {
        this.componentsService.update(this.editableModel.id, this.editableModel).subscribe(() => {
          this.OnChange.emit(true);
        });
      } else {
        this.componentsService.create(this.editableModel).subscribe(() => {
          this.OnChange.emit(true);
        });
      }
    }
  }

  public delete(model: ComponentModel): void {
    this.componentsService.delete(model.id).subscribe(() => {
      this.OnChange.emit(true);
    });
  }

  public formKeyPress(e): void {
    switch (e.keyCode) {
      case 13: // Enter
        this.save();
        break;

      case 27: // ESC
        this.reset();
        break;
    }
  }
}
