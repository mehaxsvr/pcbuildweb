import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MainService} from './main.service';
import {FieldModel} from '../models/field.model';

@Injectable({
  providedIn: 'root'
})
export class FieldsService extends MainService<FieldModel> {

  constructor(protected http: HttpClient) {
    super('fields');
  }
}
