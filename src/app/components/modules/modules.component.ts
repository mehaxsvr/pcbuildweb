import {Component, OnInit, ViewChild} from '@angular/core';
import {TypesService} from '../../services/types.service';
import {TypeModel} from '../../models/type.model';
import {TypesComponent} from './types/types.component';
import {FieldsService} from '../../services/fields.service';
import {FieldModel} from '../../models/field.model';
import {FieldsComponent} from './fields/fields.component';
import {CurrencyModel} from '../../models/currency.model';
import {CurrenciesComponent} from './currencies/currencies.component';
import {CurrenciesService} from '../../services/currencies.service';
import {StoreModel} from '../../models/store.model';
import {StoresComponent} from './stores/stores.component';
import {StoresService} from '../../services/stores.service';
import {TypeFieldModel} from '../../models/type-field.model';
import {TypeFieldsComponent} from './type-fields/type-fields.component';
import {TypeFieldsService} from '../../services/type-fields.service';
import {CaptionModel} from '../../models/caption.model';
import {CaptionsComponent} from './captions/captions.component';
import {CaptionsService} from '../../services/captions.service';
import {ProductModel} from '../../models/product.model';
import {ProductsComponent} from './products/products.component';
import {SpecificationModel} from '../../models/specification.model';
import {SpecificationsComponent} from './specifications/specifications.component';
import {SpecificationsService} from '../../services/specifications.service';
import {ProductsService} from '../../services/products.service';
import {ComponentModel} from '../../models/component.model';
import {ComponentsComponent} from './components/components.component';
import {ComponentsService} from '../../services/components.service';
import {forkJoin, Observable} from 'rxjs';

@Component({
  selector: 'app-modules',
  templateUrl: './modules.component.html',
  styleUrls: ['./modules.component.scss']
})
export class ModulesComponent implements OnInit {

  constructor(
      private typesService: TypesService,
      private fieldsService: FieldsService,
      private typeFieldsService: TypeFieldsService,
      private captionsService: CaptionsService,
      private currenciesService: CurrenciesService,
      private storesService: StoresService,
      private componentsService: ComponentsService,
      private specificationsService: SpecificationsService,
      private productsService: ProductsService
  ) {}
  types: TypeModel[];
  @ViewChild('typesComponent', {static: false}) typesComponent: TypesComponent;

  fields: FieldModel[];
  @ViewChild('fieldsComponent', {static: false}) fieldsComponent: FieldsComponent;

  typeFields: TypeFieldModel[];
  @ViewChild('typeFieldsComponent', {static: false}) typeFieldsComponent: TypeFieldsComponent;

  captions: CaptionModel[];
  @ViewChild('captionsComponent', {static: false}) captionsComponent: CaptionsComponent;

  currencies: CurrencyModel[];
  @ViewChild('currenciesComponent', {static: false}) currenciesComponent: CurrenciesComponent;

  stores: StoreModel[];
  @ViewChild('storesComponent', {static: false}) storesComponent: StoresComponent;

  components: ComponentModel[];
  @ViewChild('componentsComponent', {static: false}) componentsComponent: ComponentsComponent;

  specifications: SpecificationModel[];
  @ViewChild('specificationsComponent', {static: false}) specificationsComponent: SpecificationsComponent;

  products: ProductModel[];
  @ViewChild('productsComponent', {static: false}) productsComponent: ProductsComponent;

  public refreshTypes = new Observable<boolean>(observer => {
    this.typesService.getAll().subscribe(response => {
      this.types = response.data;
      this.typesComponent.reset();

      observer.next(true);
      observer.complete();
    });
  });

  public refreshFields = new Observable<boolean>(observer => {
    this.fieldsService.getAll().subscribe(response => {
      this.fields = response.data;
      this.fieldsComponent.reset();

      observer.next(true);
      observer.complete();
    });
  });

  public refreshCaptions = new Observable<boolean>(observer => {
    this.captionsService.getAll().subscribe(response => {
      this.captions = response.data;

      observer.next(true);
      observer.complete();
    });
  });

  public refreshTypeFields = new Observable<boolean> (observer => {
    this.typeFieldsService.getAll().subscribe(response => {
      this.typeFields = response.data;

      observer.next(true);
      observer.complete();
    });
  });

  public refreshCurrencies(): void {
    this.currenciesService.getAll().subscribe(response => {
      this.currencies = response.data;
      this.currenciesComponent.reset();
    });
  }

  public refreshStores(): void {
    this.storesService.getAll().subscribe(response => {
      this.stores = response.data;
      this.storesComponent.reset();
    });
  }

  public refreshComponents(): void {
    this.componentsService.getAll().subscribe(response => {
      this.components = response.data;
      this.componentsComponent.reset();
    });
  }

  public refreshSpecifications(): void {
    this.specificationsService.getAll().subscribe(response => {
      this.specifications = response.data;
      this.specificationsComponent.reset();
    });
  }

  public refreshProducts(): void {
    this.productsService.getAll().subscribe(response => {
      this.products = response.data;
      this.productsComponent.reset();
    });
  }

  ngOnInit() {
    this.refreshTypesAndFields();
    this.refreshCurrencies();
    this.refreshStores();
    this.refreshComponents();
    this.refreshSpecifications();
    this.refreshProducts();
  }

  public refreshTypesAndFields(): void {
    const observables = [
      this.refreshTypes,
      this.refreshFields,
      this.refreshCaptions,
      this.refreshTypeFields
    ];
    forkJoin(observables).subscribe(() => {
      this.typeFieldsComponent.reset();
      this.captionsComponent.reset();
    });
  }
}
