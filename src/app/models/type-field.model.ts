import {FieldModel} from './field.model';
import {TypeModel} from './type.model';

export interface TypeFieldModel {
  id?: number;
  type_id: number;
  field_id: number;

  field?: FieldModel;
  type?: TypeModel;
}
