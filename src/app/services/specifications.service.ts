import { Injectable } from '@angular/core';
import {MainService} from './main.service';
import {SpecificationModel} from '../models/specification.model';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SpecificationsService extends MainService<SpecificationModel> {

  constructor(protected http: HttpClient) {
    super('specifications', [
        'component',
        'field',
        'caption'
    ]);
  }
}
