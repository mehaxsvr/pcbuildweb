import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FieldsService} from '../../../services/fields.service';
import {FieldModel} from '../../../models/field.model';

@Component({
  selector: 'app-fields',
  templateUrl: './fields.component.html',
  styleUrls: ['./fields.component.scss']
})
export class FieldsComponent implements OnInit {
  @Input() models: FieldModel[];
  editableModel: FieldModel = null;

  @Output() OnChange: EventEmitter<boolean> = new EventEmitter();

  constructor(private fieldsService: FieldsService) { }

  ngOnInit() {
  }

  public setEditable(model?: FieldModel): void {
    this.editableModel = {
      id: model ? model.id : null,
      name: model ? model.name : ''
    };
  }

  public reset(): void {
    this.editableModel = null;
  }

  public save(): void {
    if (this.editableModel.name) {
      if (this.editableModel.id) {
        this.fieldsService.update(this.editableModel.id, this.editableModel).subscribe(() => {
          this.OnChange.emit(true);
        });
      } else {
        this.fieldsService.create(this.editableModel).subscribe(() => {
          this.OnChange.emit(true);
        });
      }
    }
  }

  public formKeyPress(e): void {
    switch (e.keyCode) {
      case 13: // Enter
        this.save();
        break;

      case 27: // ESC
        this.reset();
        break;
    }
  }
}

