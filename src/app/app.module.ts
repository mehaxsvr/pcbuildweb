import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {ModulesComponent} from './components/modules/modules.component';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule, Routes} from '@angular/router';
import {FormsModule} from '@angular/forms';
import { TypesComponent } from './components/modules/types/types.component';
import { FieldsComponent } from './components/modules/fields/fields.component';
import { CurrenciesComponent } from './components/modules/currencies/currencies.component';
import { StoresComponent } from './components/modules/stores/stores.component';
import { TypeFieldsComponent } from './components/modules/type-fields/type-fields.component';
import { CaptionsComponent } from './components/modules/captions/captions.component';
import { SpecificationsComponent } from './components/modules/specifications/specifications.component';
import { ProductsComponent } from './components/modules/products/products.component';
import { ComponentsComponent } from './components/modules/components/components.component';

const appRoutes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        component: ModulesComponent
    }
];

@NgModule({
    declarations: [
        AppComponent,
        ModulesComponent,
        TypesComponent,
        FieldsComponent,
        CurrenciesComponent,
        StoresComponent,
        TypeFieldsComponent,
        CaptionsComponent,
        SpecificationsComponent,
        ProductsComponent,
        ComponentsComponent
    ],
    imports: [
        RouterModule.forRoot(appRoutes),
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        FormsModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
}
