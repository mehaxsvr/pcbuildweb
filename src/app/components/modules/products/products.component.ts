import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ProductModel} from '../../../models/product.model';
import {ComponentModel} from '../../../models/component.model';
import {StoreModel} from '../../../models/store.model';
import {CurrencyModel} from '../../../models/currency.model';
import {ProductsService} from '../../../services/products.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  @Input() models: ProductModel[];
  @Input() components: ComponentModel[];
  @Input() stores: StoreModel[];
  @Input() currencies: CurrencyModel[];
  editableModel: ProductModel = null;

  @Output() OnChange: EventEmitter<boolean> = new EventEmitter();

  constructor(private productsService: ProductsService) { }

  ngOnInit() {
  }

  public setEditable(model?: ProductModel): void {
    this.editableModel = {
      id: model ? model.id : null,
      title: model ? model.title : '',
      component_id: model ? model.component_id : 1,
      store_id: model ? model.store_id : 1,
      currency_id : model ? model.currency_id  : 1,
      price: model ? model.price : 0
    };
  }

  public reset(): void {
    this.editableModel = null;
  }

  public save(): void {
    if (this.editableModel.price) {
      if (this.editableModel.id) {
        this.productsService.update(this.editableModel.id, this.editableModel).subscribe(() => {
          this.OnChange.emit(true);
        });
      } else {
        this.productsService.create(this.editableModel).subscribe(() => {
          this.OnChange.emit(true);
        });
      }
    }
  }

  public delete(model: ProductModel): void {
    this.productsService.delete(model.id).subscribe(() => {
      this.OnChange.emit(true);
    });
  }

  public formKeyPress(e): void {
    switch (e.keyCode) {
      case 13: // Enter
        this.save();
        break;

      case 27: // ESC
        this.reset();
        break;
    }
  }
}
