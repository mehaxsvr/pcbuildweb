import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MainService} from './main.service';
import {CurrencyModel} from '../models/currency.model';

@Injectable({
  providedIn: 'root'
})
export class CurrenciesService extends MainService<CurrencyModel> {

  constructor(protected http: HttpClient) {
    super('currencies');
  }
}
