import {CurrencyModel} from './currency.model';

export interface StoreModel {
  id?: number;
  name: string;
  currency_id: number;

  currency?: CurrencyModel;
}
