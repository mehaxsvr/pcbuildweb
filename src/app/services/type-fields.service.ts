import { Injectable } from '@angular/core';
import {MainService} from './main.service';
import {TypeFieldModel} from '../models/type-field.model';
import {HttpClient} from '@angular/common/http';
import {SortModel} from '../models/sort.model';

@Injectable({
  providedIn: 'root'
})
export class TypeFieldsService extends MainService<TypeFieldModel> {
  protected sorts = [
      SortModel.Create('type_id', 'ASC')
  ];

  constructor(protected http: HttpClient) {
    super('type-fields', [
      'type',
      'field'
    ]);
  }
}
