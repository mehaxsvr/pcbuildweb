import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TypeFieldsService} from '../../../services/type-fields.service';
import {TypeFieldModel} from '../../../models/type-field.model';
import {TypeModel} from '../../../models/type.model';
import {FieldModel} from '../../../models/field.model';

@Component({
  selector: 'app-type-fields',
  templateUrl: './type-fields.component.html',
  styleUrls: ['./type-fields.component.scss']
})
export class TypeFieldsComponent implements OnInit {
  @Input() models: TypeFieldModel[];
  @Input() types: TypeModel[];
  @Input() fields: FieldModel[];
  editableModel: TypeFieldModel = null;
  selectedTypeId?: number;
  selectedFieldId?: number;

  @Output() OnChange: EventEmitter<boolean> = new EventEmitter();

  constructor(private typeFieldsService: TypeFieldsService) { }

  ngOnInit() {
  }

  public setEditable(model?: TypeFieldModel): void {
    this.editableModel = {
      id: model ? model.id : null,
      field_id: model ? model.field_id : this.selectedFieldId,
      type_id: model ? model.type_id : this.selectedTypeId
    };
  }

  public isHidden(model: TypeFieldModel): boolean {
    // tslint:disable:triple-equals
    return !(
        (this.selectedFieldId == 0 || this.selectedFieldId == model.field_id) &&
        (this.selectedTypeId == 0 || this.selectedTypeId == model.type_id)
    );
  }

  public reset(): void {
    this.editableModel = null;
    this.selectedTypeId = 0;
    this.selectedFieldId = 0;
  }

  public save(): void {
    if (this.editableModel.type_id && this.editableModel.field_id) {
      if (this.editableModel.id) {
        this.typeFieldsService.update(this.editableModel.id, this.editableModel).subscribe(() => {
          this.OnChange.emit(true);
        });
      } else {
        this.typeFieldsService.create(this.editableModel).subscribe(() => {
          this.OnChange.emit(true);
        });
      }
    }
  }

  public formKeyPress(e): void {
    switch (e.keyCode) {
      case 13: // Enter
        this.save();
        break;

      case 27: // ESC
        this.reset();
        break;
    }
  }
}
