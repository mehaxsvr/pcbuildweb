import { Injectable } from '@angular/core';
import {MainService} from './main.service';
import {CaptionModel} from '../models/caption.model';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CaptionsService extends MainService<CaptionModel> {

  constructor(protected http: HttpClient) {
    super('captions', [
      'parent',
      'type',
      'field'
    ]);
  }
}
