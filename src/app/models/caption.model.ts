import {TypeModel} from './type.model';
import {FieldModel} from './field.model';

export interface CaptionModel {
    id: number;
    is_main: boolean;
    parent_id?: number;
    type_id: number;
    field_id: number;
    value: string;

    parent?: CaptionModel;
    type?: TypeModel;
    field?: FieldModel;
}
