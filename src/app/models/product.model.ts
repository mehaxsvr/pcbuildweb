import {ComponentModel} from './component.model';
import {StoreModel} from './store.model';
import {CurrencyModel} from './currency.model';

export interface ProductModel {
    id?: number;
    title: string;
    component_id: number;
    store_id: number;
    currency_id: number;
    price: number;

    component?: ComponentModel;
    store?: StoreModel;
    currency?: CurrencyModel;
}
