import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TypeFieldsComponent } from './type-fields.component';

describe('TypeFieldsComponent', () => {
  let component: TypeFieldsComponent;
  let fixture: ComponentFixture<TypeFieldsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TypeFieldsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeFieldsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
