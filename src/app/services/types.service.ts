import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MainService} from './main.service';
import {TypeModel} from '../models/type.model';

@Injectable({
    providedIn: 'root'
})
export class TypesService extends MainService<TypeModel> {
    constructor(protected http: HttpClient) {
      super('types');
    }
}
