import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {StoreModel} from '../../../models/store.model';
import {StoresService} from '../../../services/stores.service';
import {CurrencyModel} from '../../../models/currency.model';

@Component({
  selector: 'app-stores',
  templateUrl: './stores.component.html',
  styleUrls: ['./stores.component.scss']
})
export class StoresComponent implements OnInit {
  @Input() models: StoreModel[];
  @Input() currencies: CurrencyModel[];
  editableModel: StoreModel = null;

  @Output() OnChange: EventEmitter<boolean> = new EventEmitter();


  constructor(private storesService: StoresService) { }

  ngOnInit() {
  }

  public setEditable(model?: StoreModel): void {
    this.editableModel = {
      id: model ? model.id : null,
      name: model ? model.name : '',
      currency_id: model ? model.currency_id : 1,
    };
  }

  public reset(): void {
    this.editableModel = null;
  }

  public save(): void {
    if (this.editableModel.name) {
      if (this.editableModel.id) {
        this.storesService.update(this.editableModel.id, this.editableModel).subscribe(() => {
          this.OnChange.emit(true);
        });
      } else {
        this.storesService.create(this.editableModel).subscribe(() => {
          this.OnChange.emit(true);
        });
      }
    }
  }

  public formKeyPress(e): void {
    switch (e.keyCode) {
      case 13: // Enter
        this.save();
        break;

      case 27: // ESC
        this.reset();
        break;
    }
  }
}
