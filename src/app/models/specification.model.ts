import {ComponentModel} from './component.model';
import {FieldModel} from './field.model';
import {CaptionModel} from './caption.model';

export interface SpecificationModel {
    id?: number;
    component_id: number;
    field_id: number;
    caption_id: number;
    value: string;

    component?: ComponentModel;
    field?: FieldModel;
    caption?: CaptionModel;
}
