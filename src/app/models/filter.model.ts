export class FilterModel {
    key: string;
    operator: string;
    value: string;

    static Create(key: string, value: string, operator: string = 'eq'): FilterModel {
        const model = new FilterModel();
        model.key = key;
        model.operator = operator;
        model.value = value;
        return model;
    }

    public static compileParam(value: string, ... chain: any[]): string {
        const first = chain.shift();
        const key = first + chain.map(name => `[${name}]`).join('');
        return `${key}=${value}`;
    }

    toParams(groupIndex: number, filterIndex: number): string {
        return [
            FilterModel.compileParam(this.key, 'filter_group', groupIndex, 'filter', filterIndex, 'key'),
            FilterModel.compileParam(this.operator, 'filter_group', groupIndex, 'filter', filterIndex, 'operator'),
            FilterModel.compileParam(this.value, 'filter_group', groupIndex, 'filter', filterIndex, 'value'),
        ].join('&');
    }
}
