export class SortModel {
    key: string;
    direction = 'ASC';

    static Create(key: string, direction = 'ASC'): SortModel {
        const model: SortModel = new SortModel();
        model.key = key;
        model.direction = direction;
        return model;
    }
}
