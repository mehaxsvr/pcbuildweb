import {TypeModel} from './type.model';

export interface ComponentModel {
    id: number;
    type_id: number;
    name: string;

    type?: TypeModel;
}
