import { TestBed } from '@angular/core/testing';

import { CaptionsService } from './captions.service';

describe('CaptionsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CaptionsService = TestBed.get(CaptionsService);
    expect(service).toBeTruthy();
  });
});
