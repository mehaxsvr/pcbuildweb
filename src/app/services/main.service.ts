import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {ResponseModel} from '../models/response.model';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {FilterGroupModel} from '../models/filter.group.model';
import {SortModel} from '../models/sort.model';
import {FilterModel} from '../models/filter.model';

@Injectable({
  providedIn: 'root'
})
export class MainService<Model> {
  protected http: HttpClient;
  protected endpoint: string;
  protected includes: string[] = [];
  protected sorts: SortModel[] = [];
  protected filterGroups: FilterGroupModel[] = [];

  constructor(endpoint: string, includes: string[] = []) {
    this.endpoint = endpoint;
    this.includes = includes;
  }

  private get includesParams(): string {
    return this.includes.map(item => FilterModel.compileParam(item, 'includes', '')).join('&');
  }

  public getAll(): Observable<ResponseModel<Model[]>> {
    return this.http.get<ResponseModel<Model[]>>(`${environment.apiUrl}/${this.endpoint}${this.params}`);
  }

  public getById(id: number): Observable<ResponseModel<Model>> {
    return this.http.get<ResponseModel<Model>>(`${environment.apiUrl}/${this.endpoint}/${id}${this.params}`);
  }

  public create(model: Model): Observable<ResponseModel<Model>> {
    return this.http.post<ResponseModel<Model>>(`${environment.apiUrl}/${this.endpoint}`, {
      data: model
    });
  }

  public update(id: number, model: Model): Observable<ResponseModel<Model>> {
    return this.http.put<ResponseModel<Model>>(`${environment.apiUrl}/${this.endpoint}/${id}`, {
      data: model
    });
  }

  public delete(id: number): Observable<ResponseModel<any>> {
    return this.http.delete<ResponseModel<any>>(`${environment.apiUrl}/${this.endpoint}/${id}`);
  }

  private get sortParams(): string {
    return this.sorts.map((sort, index) => [
      FilterModel.compileParam(sort.key, 'sort', index, 'key'),
      FilterModel.compileParam(sort.direction, 'sort', index, 'direction'),
    ].join('&')).join('&');
  }

  private get filterGroupsParams(): string {
    return this.filterGroups.map((filterGroup, groupIndex) => filterGroup.toParams(groupIndex)).join('&');
  }

  private get params(): string {
    const parts = [];
    if (this.includes.length > 0) {
      parts.push(this.includesParams);
    }
    if (this.sorts.length > 0) {
      parts.push(this.sortParams);
    }
    if (this.filterGroups.length > 0) {
      parts.push(this.filterGroupsParams);
    }
    if (parts.length > 0) {
      return '?' + parts.join('&');
    }
    return '';
  }
}
