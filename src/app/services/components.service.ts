import { Injectable } from '@angular/core';
import {MainService} from './main.service';
import {ComponentModel} from '../models/component.model';
import {HttpClient} from '@angular/common/http';
import {SortModel} from '../models/sort.model';

@Injectable({
  providedIn: 'root'
})
export class ComponentsService extends MainService<ComponentModel> {
  protected sorts: SortModel[] = [
      SortModel.Create('type_id', 'ASC')
  ];

  constructor(protected http: HttpClient) {
    super('components', ['type']);
  }
}
