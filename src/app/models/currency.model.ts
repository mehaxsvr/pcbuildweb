export interface CurrencyModel {
  id?: number;
  name: string;
  short: string;
  to_eur: number;
}
