import { Injectable } from '@angular/core';
import {MainService} from './main.service';
import {ProductModel} from '../models/product.model';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProductsService extends MainService<ProductModel> {

  constructor(protected http: HttpClient) {
    super('products', [
        'component',
        'store',
        'currency',
    ]);
  }
}
