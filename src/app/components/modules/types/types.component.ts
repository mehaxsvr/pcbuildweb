import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TypeModel} from '../../../models/type.model';
import {TypesService} from '../../../services/types.service';

@Component({
  selector: 'app-types',
  templateUrl: './types.component.html',
  styleUrls: ['./types.component.scss']
})
export class TypesComponent implements OnInit {
  @Input() models: TypeModel[];
  editableModel: TypeModel = null;

  @Output() OnChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private typesService: TypesService) { }

  ngOnInit() {
  }

  public setEditable(model?: TypeModel): void {
    this.editableModel =  {
      id: model ? model.id : null,
      name: model ? model.name : '',
      short: model ? model.short : ''
    };
  }

  public reset(): void {
    this.editableModel = null;
  }

  public save(): void {
    if (this.editableModel.name && this.editableModel.short) {
      if (this.editableModel.id) {
        this.typesService.update(this.editableModel.id, this.editableModel).subscribe(() => {
          this.OnChange.emit(true);
        });
      } else {
        this.typesService.create(this.editableModel).subscribe(() => {
          this.OnChange.emit(true);
        });
      }
    }
  }

  public formKeyPress(e): void {
    switch (e.keyCode) {
      case 13: // Enter
        this.save();
        break;

      case 27: // ESC
        this.reset();
        break;
    }
  }
}
