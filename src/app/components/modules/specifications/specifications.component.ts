import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CaptionModel} from '../../../models/caption.model';
import {SpecificationModel} from '../../../models/specification.model';
import {ComponentModel} from '../../../models/component.model';
import {FieldModel} from '../../../models/field.model';
import {SpecificationsService} from '../../../services/specifications.service';

@Component({
  selector: 'app-specifications',
  templateUrl: './specifications.component.html',
  styleUrls: ['./specifications.component.scss']
})
export class SpecificationsComponent implements OnInit {
  @Input() models: SpecificationModel[];
  @Input() components: ComponentModel[];
  @Input() fields: FieldModel[];
  @Input() captions: CaptionModel[];
  editableModel: SpecificationModel = null;

  @Output() OnChange: EventEmitter<boolean> = new EventEmitter();

  constructor(private specificationsService: SpecificationsService) { }

  ngOnInit() {
  }

  public setEditable(model?: SpecificationModel): void {
    this.editableModel = {
      id: model ? model.id : null,
      component_id: model ? model.component_id : this.components[0].id,
      field_id: model ? model.field_id : this.fields[0].id,
      caption_id : model ? model.caption_id : null,
      value: model ? model.value : ''
    };
  }

  public reset(): void {
    this.editableModel = null;
  }

  public save(): void {
    if (this.editableModel.value) {
      if (this.editableModel.id) {
        this.specificationsService.update(this.editableModel.id, this.editableModel).subscribe(() => {
          this.OnChange.emit(true);
        });
      } else {
        this.specificationsService.create(this.editableModel).subscribe(() => {
          this.OnChange.emit(true);
        });
      }
    }
  }

  public delete(model: SpecificationModel): void {
    this.specificationsService.delete(model.id).subscribe(() => {
      this.OnChange.emit(true);
    });
  }

  public formKeyPress(e): void {
    switch (e.keyCode) {
      case 13: // Enter
        this.save();
        break;

      case 27: // ESC
        this.reset();
        break;
    }
  }
}
