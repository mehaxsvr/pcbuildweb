import {FilterModel} from './filter.model';

export class FilterGroupModel {
    or: boolean;
    filters: FilterModel[];

    static Create(... filters: FilterModel[]): FilterGroupModel {
        const model = new FilterGroupModel();
        model.or = false;
        model.filters = filters;
        return model;
    }

    get orAsString(): string {
        return this.or ? 'true' : 'false';
    }

    private filtersToParams(groupIndex: number): string {
        return this.filters.map((filter, filterIndex) => {
            return filter.toParams(groupIndex, filterIndex);
        }).join('&');
    }

    toParams(groupIndex: number): string {
        return [
            FilterModel.compileParam(this.orAsString, 'filter_group', groupIndex, 'or'),
            this.filtersToParams(groupIndex)
        ].join('&');
    }
}
