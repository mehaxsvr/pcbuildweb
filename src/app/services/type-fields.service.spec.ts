import { TestBed } from '@angular/core/testing';

import { TypeFieldsService } from './type-fields.service';

describe('TypeFieldsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TypeFieldsService = TestBed.get(TypeFieldsService);
    expect(service).toBeTruthy();
  });
});
